# Gradle Plugins

---

> This repository contains gradle plugins. These plugins can be applied in another
> gradle projects.

## Plugins

To use these plugins configure repository for plugins in **settings.gradle** file.

```
pluginManagement {
    repositories {
        // Plugins maven url
        maven {
            url 'https://gitlab.com/api/v4/projects/43975563/packages/maven'
        }
        // Platforms maven url, needed for spring boot plugins
        maven {
            url 'https://gitlab.com/api/v4/projects/43975633/packages/maven'
        }
    }
}
```

And then apply plugin in **build.gradle** file.

```
plugins {
    id "com.konigsberger.www.gradle-plugins.commons-java-plugin" version "0.0.1"
    id "com.konigsberger.www.gradle-plugins.commons-jacoco-plugin" version "0.0.1"
    id "com.konigsberger.www.gradle-plugins.spring-boot-library-plugin" version "0.0.1"
}
```

### Common Plugins

- [Java Plugin](plugins/commons/commons-java-plugin/README.md)
- [Java Library Plugin](plugins/commons/commons-java-library-plugin/README.md)
- [Docker Plugin](plugins/commons/commons-docker-plugin/README.md)
- [Git Conventional Commit Plugin](plugins/commons/commons-git-conventional-commit-plugin/README.md)

### Spring Boot Plugins

- [Spring Boot Application Plugin](plugins/spring-boot/spring-boot-application-plugin)
- [Spring Boot Library Plugin](plugins/spring-boot/spring-boot-library-plugin)